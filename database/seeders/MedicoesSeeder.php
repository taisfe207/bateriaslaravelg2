<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       DB::table('medicoes')->insert(
     [
        [
            'pilha_bateria' => 'bateria Freedom 30Ah',
            'tensao_nominal' => 12,
            'capacidade_corrente' => 26000,
            'tensao_sem_carga' => 10.68,
            'tensao_com_carga' => 10.67,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'bateria Unipower 7Ah',
            'tensao_nominal' => 12,
            'capacidade_corrente' => 7000,
            'tensao_sem_carga' => 10.47,
            'tensao_com_carga' => 10.38,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha Duracel AA',
            'tensao_nominal' => 1.5,
            'capacidade_corrente' => 2800,
            'tensao_sem_carga' => 1.306,
            'tensao_com_carga' => 1.291,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha golite',
            'tensao_nominal' => 9,
            'capacidade_corrente' => 500,
            'tensao_sem_carga' => 5.54,
            'tensao_com_carga' => 2.49,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha panasonic AA',
            'tensao_nominal' => 1.5,
            'capacidade_corrente' => 2500,
            'tensao_sem_carga' => 1.379,
            'tensao_com_carga' => 1.338,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha Luatex',
            'tensao_nominal' => 3.7,
            'capacidade_corrente' => 1200,
            'tensao_sem_carga' => 2.479,
            'tensao_com_carga' => 2.347,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha JYX',
            'tensao_nominal' => 1.5,
            'capacidade_corrente' => 9800,
            'tensao_sem_carga' => 2.564,
            'tensao_com_carga' => 2.236,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha elgin',
            'tensao_nominal' => 9,
            'capacidade_corrente' => 250,
            'tensao_sem_carga' => 7.11,
            'tensao_com_carga' => 2.85,
            'resistencia_carga' => 23.000,
        ],
        [
            'pilha_bateria' => 'Pilha Philips',
            'tensao_nominal' => 1.5,
            'capacidade_corrente' => 1200,
            'tensao_sem_carga' => 1.370,
            'tensao_com_carga' => 1.307,
            'resistencia_carga' => 23.000,
        ],



     ] 
        );
       //
        }
}
